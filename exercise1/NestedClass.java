package exercise1;

class DefaultClass {

}

public class NestedClass {
    int i;
    class InnerClass{
        public void method(){
            i = 5;
        }
    }

    static class StaticNestedClass {
//        public void method(){
//            i = 5; // making a static ref to an non-static field (not allowed)
//        }
    }

    public static void main(String[] args) {
        StaticNestedClass staticNestedClass = new StaticNestedClass();

        NestedClass nestedClass = new NestedClass();
        InnerClass innerClass = nestedClass.new InnerClass();

    }
}
