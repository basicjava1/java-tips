package exercise1;

class Player {
    private String name;
    private static int count;

    public Player(String name){
        super();
        this.name = name;
        count++;
    }

    public static int getCount() {
        return count;
    }
}

public class StaticUseCase {
    public static void main(String[] args) {
        Player player1 = new Player("Messi");
        System.out.println(Player.getCount());

        Player player2 = new Player("ROnaldo");
        System.out.println(Player.getCount());
    }
}
