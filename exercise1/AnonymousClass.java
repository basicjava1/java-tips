package exercise1;

import java.util.*;

class LengthComparator implements Comparator<String> {
    public int compare(String str1, String str2){
        return Integer.compare(str1.length(), str2.length());
    }
}

public class AnonymousClass {
    public static void main(String[] args) {
        List<String> animals = new ArrayList<>(List.of("Ant","Bat","Cat","dog"));
        Collections.sort(animals, new Comparator<String>() {

            @Override
            public int compare(String str1, String str2) {
                return Integer.compare(str1.length(), str2.length());
            }

        });
        System.out.println(animals);
    }

}
